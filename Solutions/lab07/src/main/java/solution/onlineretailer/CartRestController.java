package solution.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
public class CartRestController {
    @Autowired
    private Map<Integer, Item> catalog;

    @Autowired
    private CartService cartService;

    @GetMapping(value= "/items", produces={"application/json","application/xml"})
    public List<Item> getAllItems(){
        Set<Integer> itemKeys = cartService.getAllItemsInCart().keySet();
        List<Item> allItems = null;
        for (int key: itemKeys){
            Item itemValue = catalog.get(key);
            allItems.add(itemValue);
        }
        return allItems;}

    @GetMapping(value="/cartCost", produces={"application/json","application/xml"})
    public double getCartCost() {
        double totalCost = cartService.calculateCartCost();
        return totalCost;
    }
    @GetMapping(value="quantityForItem/{itemId}", produces={"application/json","application/xml"})
    public int getQuantityForItem(@PathVariable int itemId){
    int quantity = cartService.getAllItemsInCart().get(itemId);
    if (quantity!= 0){ return quantity; } else{ return 0;}
    }


}
