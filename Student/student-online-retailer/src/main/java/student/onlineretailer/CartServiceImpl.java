package student.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Map;

@Component
public class CartServiceImpl implements CartService{

    @Value("#{catalog}")
    private Map<Integer, Item> catalog;

    @Value("customerService@companyName.com")
    private String contactEmail;

    @Value("${onlineRetailers.salesTaxRate}")
    private double salesTaxRate;

    @Value("${onlineRetailers.deliveryCharge.normal}")
    private double normalDelivery;

    @Value("${onlineRetailers.deliveryCharge.threshold}")
    private double thresholdDelivery;


    @Autowired
    private CartRepository repo;

    // getters and setters
    public String getContactEmail() { return contactEmail; }
    public double getSalesTaxRate() { return salesTaxRate;}
    public double getNormalDelivery() {return normalDelivery; }
    public double getThresholdDelivery() { return thresholdDelivery;}

    //methods
    @Override
    public void addItemToCart(int id, int quantity) {

        if (catalog.containsKey(id)){
            repo.add(id, quantity);
        }

    }

    @Override
    public void removeItemFromCart(int id) {
        repo.remove(id);

    }

    @Override
    public Map<Integer, Integer> getAllItemsInCart() {
        return repo.getAll();
    }

    @Override
    public double calculateCartCost() {
        Map<Integer,Integer> allItems = repo.getAll();

        double totalCost = 0;
        for (Map.Entry<Integer, Integer> entry: allItems.entrySet()){
            int id = entry.getKey();
            int quantity = entry.getValue();

            double price = catalog.get(id).getPrice() * quantity;
            totalCost += price;

        }
        return totalCost;
    }
}
