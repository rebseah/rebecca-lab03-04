package student.onlineretailer;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CartRepositoryImpl implements CartRepository{

    //instance variables
    private Map<Integer,Integer> cart = new HashMap<>();
    @Override
    public void add(int itemId, int quantity) {
        if (cart.containsKey(itemId)){
            int currentQuantity = cart.get(itemId);
            int newQuantity = currentQuantity + quantity;
            cart.replace(itemId, currentQuantity, newQuantity );
        }
        else{
            cart.put(itemId, quantity);
        }
    }

    @Override
    public void remove(int itemId) {
        if (cart.containsKey(itemId)){
            cart.remove(itemId);
        }

    }

    @Override
    public Map<Integer, Integer> getAll() {
        return cart;
    }
}
